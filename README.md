# 📦 Cidenet-app

[![License: MIT](https://img.shields.io/badge/License-MIT-blue.svg)](https://opensource.org/licenses/MIT)

## Instalación

Para clonar el repositorio escoja: SSH o HTTPS

Una vez clonado el repositorio ejecute

```bash
$ composer install
```

Ejecute las migraciones y Seeders 

```bash
$ php artisan migrate --seed
```

Seguido instale las dependecias de Node

```bash
$ npm install
```

Para compilar los recursos ejecute

```bash
$ npm run dev
```

### Actualización

Para actualizar las dependencias de PHP y Node ejecute
```bash
$ composer update
$ npm update
$ npm dev
```

### Configuración

- Renombrar el `env` por `.env`
- Configure las variables de entorno

### Requerimientos del Servidor

Se requiere PHP 8.0.2 con las siguientes extensiones instaladas: 

- [intl](http://php.net/manual/en/intl.requirements.php)
- [libcurl](http://php.net/manual/en/curl.requirements.php) if you plan to use the HTTP\CURLRequest library

Además, asegúrese de que las siguientes extensiones estén habilitadas en su PHP:

- json (enabled by default - don't turn it off)
- [mbstring](http://php.net/manual/en/mbstring.installation.php)
- [mysqlnd](http://php.net/manual/en/mysqlnd.install.php)
- xml (enabled by default - don't turn it off)