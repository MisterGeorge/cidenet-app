<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\Document;
use App\Models\Area;
use App\Http\Requests\StoreUserRequest;
use App\Http\Requests\UpdateUserRequest;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::all();

        return view('users.index', compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $documents = Document::all();
        $areas     = Area::all();

        return view('users.create', compact('documents', 'areas'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreUserRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreUserRequest $request)
    {
        $validatedData = $request->validated();

        User::create($validatedData->all());

        return redirect()->route('users.index')->with('success','Usuario creado exitosamente !!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        return view('users.show',compact('user'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        return view('users.edit',compact('user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateUserRequest  $request
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateUserRequest $request, User $user)
    {
        $validatedData = $request->validated();

        $user->updateOrCreate($validatedData->all());

        return redirect()->route('users.index')->with('success','Usuario updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        $user->delete();

        return redirect()->route('users.index')->with('success','user deleted successfully');
    }
                
    /**
     * generateEmail
     *
     * @param  mixed $first_name
     * @param  mixed $first_lastname
     * @param  mixed $email
     * @param  mixed $country
     * @return void
     */
    public function generateEmail(String $first_name, String $first_lastname, String $email, String $country)
    {
        $currentEmail = User::email($email);

        if ( $country === 'colombia' )
            $domain = 'cidenet.com.co';
        
        if ( $country === 'estados unidos' )
            $domain = 'cidenet.com.us';

        if ( $currentEmail )
            $newEmail = $first_name . '.' . $first_lastname . '@' . $domain;
        

        return $newEmail;
    }
}
