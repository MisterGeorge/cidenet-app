<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'first_name'      => 'required|regex:/([A-Z])\w+/i|max:20',
            'second_name'     => 'required|regex:/([A-Z])\w+/i|max:50',
            'first_lastname'  => 'required|regex:/([A-Z])\w+/i|max:20',
            'second_lastname' => 'required|regex:/([A-Z])\w+/i|max:20',
            'country'         => 'required|regex:/([A-Z])\w+/i',
            'document_id'     => 'required',
            'number_document' => 'required|numeric',
            'area_id'         => 'required'
        ];
    }

    /**
     * Get custom attributes for validator errors.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            'first_name'      => 'Primer Nombre',
            'second_name'     => 'Segundo nombre',
            'first_lastname'  => 'Primer Apellido',
            'second_lastname' => 'Otro Nombre',
            'country'         => 'País',
            'document_id'     => 'Tipo de Documento',
            'number_document' => 'Número de Documento',
            'area_id'         => 'Área'
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'first_name.required'     => 'El :attribute es requerido',
            'first_name.max'          => 'Tamaño máximo del :attribute es de 40 caracteres',
            'first_name.regex'        => 'El :attribute debe contener carácteres alfabéticos',
            'second_name.required'    => 'El :attribute es requerido',
            'second_name.max'         => 'Tamaño máximo del :attribute es de 50 caracteres',
            'second_name.regex'       => 'El :attribute debe contener carácteres alfabéticos',
            'first_lastname.required' => 'El :attribute es requerido',
            'first_lastname.max'      => 'Tamaño máximo del :attribute es de 20 caracteres',
            'first_lastname.regex'    => 'El :attribute debe contener carácteres alfabéticos',
            'second_lastname.required'=> 'El :attribute es requerido',
            'second_lastname.max'     => 'Tamaño máximo del :attribute es de 20 caracteres',
            'second_lastname.regex'   => 'El :attribute debe contener carácteres alfabéticos',
            'country.required'        => 'El :attribute es requerido',
            'country.regex'           => 'El :attribute debe contener carácteres alfabéticos',
            'document_id.required'    => 'El :attribute es requerido',
            'number_document.required'=> 'El :attribute es requerido',
            'number_document.numeric' => 'El :attribute debe contener carácteres numéricos',
            'area_id.required'        => 'El :attribute es requerido'
        ];
    }
}
