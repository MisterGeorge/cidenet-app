<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;
use App\Models\Area;
use App\Models\Document;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'users';

    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * Indicates if the model's ID is auto-incrementing.
     *
     * @var bool
     */
    public $incrementing = true;

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'first_name',
        'second_name',
        'first_lastname',
        'second_lastname',
        'country',
        'document_id',
        'number_document',
        'email',
        'date_admission',
        'area_id',
        'status'
    ];
  
    /**
     * document
     *
     * @return void
     */
    public function document()
    {
        return $this->hasOne(Document::class, 'foreign_key', 'local_key');
    }
    
    /**
     * area
     *
     * @return void
     */
    public function area()
    {
        return $this->hasOne(Area::class, 'foreign_key', 'local_key');
    }
    
    /**
     * scopeFirstName
     *
     * @param  mixed $query
     * @param  mixed $first_name
     * @return void
     */
    public function scopeFirstName($query, $first_name)
    {
        if (trim($first_name) || !empty($first_name))
            return $query->where('first_name', 'LIKE', '%'.$first_name.'%')
                         ->orderBy('first_name', 'asc');
    }

    /**
     * scopeSecondName
     *
     * @param  mixed $query
     * @param  mixed $second_name
     * @return void
     */
    public function scopeSecondName($query, $second_name)
    {
        if (trim($second_name) || !empty($second_name))
            return $query->where('second_name', 'LIKE', '%'.$second_name.'%')
                         ->orderBy('second_name', 'asc');
    }
    
    /**
     * scopeFirstLastName
     *
     * @param  mixed $query
     * @param  mixed $first_lastname
     * @return void
     */
    public function scopeFirstLastName($query, $first_lastname)
    {
        if (trim($first_lastname) || !empty($first_lastname))
            return $query->where('first_lastname', 'LIKE', '%'.$first_lastname.'%')
                         ->orderBy('first_lastname', 'asc');
    }

    /**
     * scopeEmail
     *
     * @param  mixed $query
     * @param  mixed $email
     * @return void
     */
    public function scopeEmail($query, $email)
    {
        if (trim($email) || !empty($email))
            return $query->where('email', 'LIKE', '%'.$email.'%');
    }

    /**
     * scopeTypeDocument
     *
     * @param  mixed $query
     * @param  mixed $document_id
     * @return void
     */
    public function scopeTypeDocument($query, $document_id)
    {
        if (trim($document_id) || !empty($document_id))
            return $query->where('document_id', 'LIKE', '%'.$document_id.'%');
    }

    /**
     * scopeNumberDocument
     *
     * @param  mixed $query
     * @param  mixed $number_document
     * @return void
     */
    public function scopeNumberDocument($query, $number_document)
    {
        if (trim($number_document) || !empty($number_document))
            return $query->where('number_document', 'LIKE', '%'.$number_document.'%');
    }

    /**
     * scopeStatus
     *
     * @param  mixed $query
     * @param  mixed $status
     * @return void
     */
    public function scopeStatus($query, $status)
    {
        if (trim($status) || !empty($status))
            return $query->where('status', 'LIKE', '%'.$status.'%');
    }
}
