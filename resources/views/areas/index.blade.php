@extends('layouts.app')

@section('title', 'Listado de Áreas')

@section('content')

    @include('partials.notification')

    <div class="row">
        <div class="col">
            <div class="card shadow-sm">
                <div class="card-header text-bg-dark">
                    <div class="float-start">
                        <h4><i class="fa-solid fa-list"></i> Áreas</h4>
                    </div>
                    <div class="float-end">
                        <a class="btn btn-outline-primary" href="{{ route('areas.create') }}">
                            <i class="fa-solid fa-plus"></i> Nuevo
                        </a>
                    </div>
                </div>
                <div class="card-body">
                    <table class="table table-hover">
                        <thead>
                            <tr class="text-center">
                                <th>#</th>
                                <th>Área</th>
                                <th>Creado</th>
                                <th>Editado</th>
                                <th>Acción</th>
                            </tr>
                        </thead>
                        <tbody>
                            @forelse ($areas as $area)
                                <tr>
                                    <td class="text-center">{{ $area->id }}</td>
                                    <td class="text-center">{{ $area->name }}</td>
                                    <td class="text-center">
                                        <span class="badge text-bg-success">{{ $area->created_at }}</span>
                                    </td>
                                    <td class="text-center">
                                        <span class="badge text-bg-warning">{{ $area->updated_at }}</span>
                                    </td>
                                    <td class="d-flex justify-content-center">
                                        <a class="btn btn-outline-info btn-sm me-1" href="{{ route('areas.show',$area->id) }}">
                                            <i class="fa-solid fa-eye"></i>
                                        </a>
                                        <a class="btn btn-outline-primary btn-sm me-1" href="{{ route('areas.edit',$area->id) }}">
                                            <i class="fa-solid fa-pen"></i>
                                        </a>
                                        <a class="btn btn-outline-danger btn-sm me-1" href="{{ route('areas.edit',$area->id) }}" data-bs-toggle="modal" data-bs-target="#deleteModal_{{ $area->id }}">
                                            <i class="fa-solid fa-trash-can"></i>
                                        </a>
                                    </td>
                                </tr>

                                @include('areas.delete')
                            @empty
                                <tr>
                                    <td colspan="5">
                                        <p class="text-dark text-center"> Sin áreas registradas! </p>
                                    </td>
                                </tr>
                            @endforelse
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

@endsection