@extends('layouts.app')

@section('title', 'Detalle del área')

@section('content')

    <div class="row">
        <div class="col-md-6 offset-md-3">
            <div class="card shadow-sm">
                <div class="card-header text-bg-dark">
                    <div class="float-start">
                        <a class="btn btn-outline-primary" href="{{ route('areas.index') }}">
                            <i class="fa-solid fa-arrow-rotate-left"></i> Atrás
                        </a>
                    </div>
                    <div class="float-end">
                        <h4>Área</h4>
                    </div>
                </div>
                <div class="card-body">
                    <div class="mb-3 row">
                        <label for="name" class="col-sm-2 col-form-label">Nombre</label>
                        <div class="col-sm-10">
                            <input type="text" name="name" class="form-control" value="{{ $area ? $area->name : '' }}" readonly disabled>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection