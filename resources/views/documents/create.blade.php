@extends('layouts.app')

@section('title', 'Documento nuevo')

@section('content')

    <div class="row">
        <div class="col-md-6 offset-md-3">
            <div class="card shadow-sm">
                <div class="card-header text-bg-dark">
                    <div class="float-start">
                        <a class="btn btn-outline-primary" href="{{ route('documents.index') }}">
                            <i class="fa-solid fa-arrow-rotate-left"></i> Atrás
                        </a>
                    </div>
                    <div class="float-end">
                        <h4>Nuevo Documento</h4>
                    </div>
                </div>
                <div class="card-body">
                    <form action="{{ route('documents.store') }}" method="POST" class="needs-validation" novalidate>
                        @csrf

                        <div class="mb-3 row">
                            <label for="name" class="col-sm-2 col-form-label">Nombre</label>
                            <div class="col-sm-10">
                                <input type="text" name="name" class="form-control @error('name') is-invalid @enderror" placeholder="Nombre del documento">
                                @error('name')
                                    <div class="invalid-feedback">{{$message}}</div>
                                @enderror
                            </div>
                        </div>

                        <div class="mb-3 row justify-content-end">
                            <div class="col-4">
                                <div class="d-grid gap-2">
                                    <button type="submit" class="btn btn-primary">
                                        <i class="fa-solid fa-floppy-disk"></i> Agregar
                                    </button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection