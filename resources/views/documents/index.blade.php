@extends('layouts.app')

@section('title', 'Listado de Documentos')

@section('content')

    @include('partials.notification')

    <div class="row">
        <div class="col">
            <div class="card shadow-sm">
                <div class="card-header text-bg-dark">
                    <div class="float-start">
                        <h4><i class="fa-solid fa-list"></i> Documentos</h4>
                    </div>
                    <div class="float-end">
                        <a class="btn btn-outline-primary" href="{{ route('documents.create') }}">
                            <i class="fa-solid fa-plus"></i> Nuevo
                        </a>
                    </div>
                </div>
                <div class="card-body">
                    <table class="table table-hover">
                        <thead>
                            <tr class="text-center">
                                <th>#</th>
                                <th>Tipo documento</th>
                                <th>Creado</th>
                                <th>Editado</th>
                                <th>Acción</th>
                            </tr>
                        </thead>
                        <tbody>
                            @forelse ($documents as $document)
                                <tr>
                                    <td class="text-center">{{ $document->id }}</td>
                                    <td class="text-center">{{ $document->name }}</td>
                                    <td class="text-center">
                                        <span class="badge text-bg-success">{{ $document->created_at }}</span>
                                    </td>
                                    <td class="text-center">
                                        <span class="badge text-bg-warning">{{ $document->updated_at }}</span>
                                    </td>
                                    <td class="d-flex justify-content-center">
                                        <a class="btn btn-outline-info btn-sm me-1" href="{{ route('documents.show',$document->id) }}">
                                            <i class="fa-solid fa-eye"></i>
                                        </a>
                                        <a class="btn btn-outline-primary btn-sm me-1" href="{{ route('documents.edit',$document->id) }}">
                                            <i class="fa-solid fa-pen"></i>
                                        </a>
                                        <a class="btn btn-outline-danger btn-sm me-1" href="{{ route('documents.edit',$document->id) }}" data-bs-toggle="modal" data-bs-target="#deleteModal_{{ $document->id }}">
                                            <i class="fa-solid fa-trash-can"></i>
                                        </a>
                                    </td>
                                </tr>

                                @include('documents.delete')
                            @empty
                                <tr>
                                    <td colspan="5">
                                        <p class="text-dark text-center"> Sin documentos registrados! </p>
                                    </td>
                                </tr>
                            @endforelse
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

@endsection