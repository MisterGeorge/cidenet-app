@extends('layouts.app')

@section('title', 'Inicio')

@section('content')

    <div class="row">
        <div class="col">
            <div class="card">
                <div class="card-body p-3 d-flex align-items-center">
                    <div class="bg-primary text-white p-3 me-3">
                        <i class="fa-solid fa-users fa-2xl"></i>
                    </div>
                    <div>
                        <div class="fs-6 fw-semibold text-primary">{{ $users }}</div>
                        <div class="text-medium-emphasis text-uppercase fw-semibold small">Usuarios</div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col">
            <div class="card">
                <div class="card-body p-3 d-flex align-items-center">
                    <div class="bg-primary text-white p-3 me-3">
                        <i class="fa-solid fa-id-card fa-2xl"></i>
                    </div>
                    <div>
                        <div class="fs-6 fw-semibold text-primary">{{ $documents }}</div>
                        <div class="text-medium-emphasis text-uppercase fw-semibold small">Documentos</div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col">
            <div class="card">
                <div class="card-body p-3 d-flex align-items-center">
                    <div class="bg-primary text-white p-3 me-3">
                        <i class="fa-solid fa-building fa-2xl"></i>
                    </div>
                    <div>
                        <div class="fs-6 fw-semibold text-primary">{{ $areas }}</div>
                        <div class="text-medium-emphasis text-uppercase fw-semibold small">Áreas</div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="p-2 mt-4 bg-light rounded-3">
        <div class="container-fluid py-3">
            <div class="row">
                <div class="col-8">
                    <h1 class="display-5 fw-bold">Sistema de gestión de empleados</h1>
                    <p class="fs-4">Sistema que permita registrar el ingreso y la salida de empleados, así como administrar de su información.</p>
                </div>
                <div class="col-4">
                    <div class="mb-3">
                        <label for="search" class="form-label">Buscar</label>
                        <input type="text" name="search" class="form-control" placeholder="Buscar...">
                    </div>
                </div>
</div>
         </div>
    </div>

@endsection