<footer class="footer mt-auto py-3 bg-light">
    <div class="container">
        <div class="float-start">
            <span class="text-muted">Sistema de gestión de empleados de <strong>Cidente S.A.S</strong>.</span>
        </div>
        <div class="float-end ml-4 text-center text-sm text-gray-500 sm:text-right sm:ml-0">
            Laravel v{{ Illuminate\Foundation\Application::VERSION }} (PHP v{{ PHP_VERSION }})
        </div>
    </div>
</footer>