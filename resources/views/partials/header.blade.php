<header>
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark" aria-label="Eighth navbar example">
        <div class="container">
            <a class="navbar-brand" href="{{ route('home') }}">
                <img src="{{ asset('images/cidenet-logo.png') }}" class="img-fluid  w-25 h-25" alt="Logo de Cidenet S.A.S">
            </a>
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbar" aria-controls="navbar" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbar">
                <ul class="navbar-nav ms-auto mb-2 mb-lg-0">
                    <li class="nav-item">
                        <a href="{{ route('home') }}" class="nav-link {{ ! Route::is('home') ?: 'active' }}">
                            <i class="fa-solid fa-house"></i> Inicio
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="{{ route('areas.index') }}" class="nav-link {{ ! Route::is('areas.*') ?: 'active' }}">
                            <i class="fa-solid fa-building"></i> Áreas
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="{{ route('documents.index') }}" class="nav-link {{ ! Route::is('documents.*') ?: 'active' }}">
                            <i class="fa-solid fa-id-card"></i> Documentos
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="{{ route('users.index') }}" class="nav-link {{ ! Route::is('users.*') ?: 'active' }}">
                            <i class="fa-solid fa-users"></i> Usuarios
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
</header>