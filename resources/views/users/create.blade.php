@extends('layouts.app')

@section('title', 'Usuario nuevo')

@section('content')

    <div class="row">
        <div class="col">
            <div class="card shadow-sm">
                <div class="card-header text-bg-dark">
                    <div class="float-start">
                        <a class="btn btn-outline-primary" href="{{ route('users.index') }}">
                            <i class="fa-solid fa-arrow-rotate-left"></i> Atrás
                        </a>
                    </div>
                    <div class="float-end">
                        <h4>Nuevo Usuario</h4>
                    </div>
                </div>
                <div class="card-body">
                    <form action="{{ route('users.store') }}" method="POST" class="needs-validation" novalidate>
                        @csrf

                        <div class="row">
                            <div class="col">
                                <div class="mb-3">
                                    <label for="first_name" class="form-label">Primer Nombre</label>
                                    <input type="text" name="first_name" class="form-control @error('first_name') is-invalid @enderror" placeholder="Primer Nombre">
                                    @error('first_name')
                                        <div class="invalid-feedback">{{$message}}</div>
                                    @enderror
                                </div>
                            </div>
                            <div class="col">
                                <div class="mb-3">
                                    <label for="second_name" class="form-label">Segundo Nombre</label>
                                    <input type="text" name="second_name" class="form-control @error('second_name') is-invalid @enderror" placeholder="Otro Nombre">
                                    @error('second_name')
                                        <div class="invalid-feedback">{{$message}}</div>
                                    @enderror
                                </div>
                            </div>
                            <div class="col">
                                <div class="mb-3">
                                    <label for="first_lastname" class="form-label">Primer Apellido</label>
                                    <input type="text" name="first_lastname" class="form-control @error('first_lastname') is-invalid @enderror" placeholder="Primer Apellido">
                                    @error('first_lastname')
                                        <div class="invalid-feedback">{{$message}}</div>
                                    @enderror
                                </div>
                            </div>
                            <div class="col">
                                <div class="mb-3">
                                    <label for="second_lastname" class="form-label">Segundo Apellido</label>
                                    <input type="text" name="second_lastname" class="form-control @error('second_lastname') is-invalid @enderror" placeholder="Segundo Apellido">
                                    @error('second_lastname')
                                        <div class="invalid-feedback">{{$message}}</div>
                                    @enderror
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                                <div class="mb-3">
                                    <label for="country" class="form-label">País</label>
                                    <select class="form-select @error('country') is-invalid @enderror" aria-label="country">
                                        <option value="">Seleccionar</option>
                                        <option value="colombia">Colombia</option>
                                        <option value="estados unidos">Estados Unidos</option>
                                    </select>
                                    @error('country')
                                        <div class="invalid-feedback">{{$message}}</div>
                                    @enderror
                                </div>
                            </div>
                            <div class="col">
                                <div class="mb-3">
                                    <label for="document_id" class="form-label">Tipo de documento</label>
                                    <select class="form-select @error('document_id') is-invalid @enderror" aria-label="country">
                                        <option value="">Seleccionar</option>
                                        @foreach ($documents as $document)
                                            <option value="{{ $document->id }}" @selected(old('document->id') == $document->id)>
                                                {{ $document->name }}
                                            </option>
                                        @endforeach
                                    </select>
                                    @error('document_id')
                                        <div class="invalid-feedback">{{$message}}</div>
                                    @enderror
                                </div>
                            </div>
                            <div class="col">
                                <div class="mb-3">
                                    <label for="number_document" class="form-label">Número de documento</label>
                                    <input type="text" name="number_document" class="form-control @error('number_document') is-invalid @enderror" placeholder="Número de documento">
                                    @error('number_document')
                                        <div class="invalid-feedback">{{$message}}</div>
                                    @enderror
                                </div>
                            </div>
                            <div class="col">
                                <div class="mb-3">
                                    <label for="area_id" class="form-label">Área</label>
                                    <select class="form-select @error('area_id') is-invalid @enderror" aria-label="area_id">
                                        <option value="">Seleccionar</option>
                                        @foreach ($areas as $area)
                                            <option value="{{ $area->id }}" @selected(old('area->id') == $area->id)>
                                                {{ $area->name }}
                                            </option>
                                        @endforeach
                                    </select>
                                    @error('area_id')
                                        <div class="invalid-feedback">{{$message}}</div>
                                    @enderror
                                </div>
                            </div>
                        </div>
                        <div class="row justify-content-end">
                            <div class="col-3">
                                <div class="d-grid gap-2">
                                    <button type="submit" class="btn btn-primary">
                                        <i class="fa-solid fa-floppy-disk"></i> Agregar
                                    </button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection