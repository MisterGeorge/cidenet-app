@extends('layouts.app')

@section('title', 'Lista de Usuarios')

@section('content')

    <div class="row">
        <div class="col">
            @if ($message = Session::get('success'))
                <div class="alert alert-success alert-dismissible fade show" role="alert">
                    <p class="mb-0">
                        <i class="fa-solid fa-check-double"></i> {{ $message }}
                    </p>
                    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                </div>
            @endif
        </div>
    </div>

    <div class="row">
        <div class="col">
            <div class="card shadow-sm">
                <div class="card-header text-bg-dark">
                    <div class="float-start">
                        <h4><i class="fa-solid fa-list"></i> Usuarios</h4>
                    </div>
                    <div class="float-end">
                        <a class="btn btn-outline-primary" href="{{ route('users.create') }}">
                            <i class="fa-solid fa-plus"></i> Nuevo
                        </a>
                    </div>
                </div>
                <div class="card-body">
                    <table class="table table-hover">
                        <thead>
                            <tr class="text-center">
                                <th>#</th>
                                <th>Primer Nombre</th>
                                <th>Segundo Nombre</th>
                                <th>Primer Apellido</th>
                                <th>Otros Nombres</th>
                                <th>País</th>
                                <th>Tipo documento</th>
                                <th># de documento</th>
                                <th>Correo</th>
                                <th>Fecha de ingreso</th>
                                <th>Área</th>
                                <th>Estado</th>
                                <th>Fecha Creado</th>
                                <th>Hora Creado</th>
                                <th>Acción</th>
                            </tr>
                        </thead>
                        <tbody>
                           
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

@endsection