<?php

namespace Tests\Unit;

use Tests\TestCase;
use App\Models\User;

class UserTest extends TestCase
{
    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function test_an_user_has_one_area()
    {
        $area = User::find(1)->area()->get();
        $this->assertTrue($area);
    }
}
